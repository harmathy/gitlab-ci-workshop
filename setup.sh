#!/bin/bash

set -e

PRIVILEGE_ESCALATION_CMD=sudo
SSH_PORT=2221
HTTP_PORT=5800

function interactive_continue {
  local message="$1"
  local command
  echo -ne "$message (Y)es/(N)o/(A)bort\n> "
  read -re command
  case $command in
    y | Y)
      return 0
      ;;
    n | N)
      return 1
      ;;
    a | A)
      exit 1
      ;;
    *)
      interactive_continue "$message"
      ;;
  esac
}

here="$(dirname "$(realpath "$0")")"
echo -e "\E[94m
░█▀▀░▀█▀░▀█▀░█░░░█▀█░█▀▄░░░█▀▀░▀█▀░░░█░█░█▀█░█▀▄░█░█░█▀▀░█░█░█▀█░█▀█
░█░█░░█░░░█░░█░░░█▀█░█▀▄░░░█░░░░█░░░░█▄█░█░█░█▀▄░█▀▄░▀▀█░█▀█░█░█░█▀▀
░▀▀▀░▀▀▀░░▀░░▀▀▀░▀░▀░▀▀░░░░▀▀▀░▀▀▀░░░▀░▀░▀▀▀░▀░▀░▀░▀░▀▀▀░▀░▀░▀▀▀░▀░░
\E[0m"
echo "This is the interactive setup script for the Gitlab CI workshop."

echo -e "\n\E[96m"
cat "$here/img/overview.ditaa"
echo -e "\n\E[0m"

echo "We will set up three docker containers via docker-compose:"
echo " * A Gitlab instance, which exposes port $SSH_PORT for SSH and $HTTP_PORT for HTTP"
echo " * A Gitlab Runner, which executes CI jobs"
echo " * A Container Registry, which stores container images (exposes port 5000)"

echo -e "\n[\E[93mStep 1\E[0m] Pre-Configuration"

echo "* Checking docker-compose"
if ! type docker-compose >/dev/null 2>&1
then
  if [ -f /etc/os-release ] && \
     (source /etc/os-release && [ "$ID" = "debian" ] && ((VERSION_ID >= 11)))
  then
    interactive_continue "Try to install it via apt-get?"
    "$PRIVILEGE_ESCALATION_CMD" apt-get install docker-compose
  else
    echo -e "Please install docker and docker-compose. Use the equivalent of\n" \
      "\E[94m# apt-get install docker-compose\E[0m\nfor your distribution."
    interactive_continue "If docker-compose is installed, continue?"
  fi
fi

echo -n "* Checking, if user is in group docker: "
if groups "$USER" | grep -q '\bdocker\b'
then
  echo "Ok"
else
  echo "No"
  interactive_continue "Add the current user \E[94m$USER\E[0m to group \E[94mdocker\E[0m?"
  "$PRIVILEGE_ESCALATION_CMD" usermod -aG docker "$USER"
  echo -e "Please \E[94mlogout and login again\E[0m for the new group membership to take effect."
  echo "Then re-run this script!"
  exit 0
fi

if ! [ "$here" = "$(realpath "$PWD")" ]
then
  echo "* Changing working directory to $(realpath --relative-to="$PWD" "$here")"
  cd "$here"
fi

echo "The Gitlab Runner requires the configuration file to exist."
config_file="runner-config/config.toml"
if [ -f "$config_file" ]
then
  echo "The file \"$config_file\" already exists. Good!"
else
  echo "* Creating file \"$config_file\"."
  touch "$config_file"
fi

echo -e "\nThe environment file has to be setup"
env_file=".env"
if [ -f "$env_file" ]
then
  host_name=$(
    # shellcheck disable=SC1090
    source "$env_file"
    # shellcheck disable=SC2153
    echo "$HOST_NAME"
  )
fi
host_name_ok=''
while [ -z "$host_name_ok" ]
do
  if [ "$host_name" = "localhost" ]
  then
    echo -e "\E[91mHostname can't be localhost\E[0m"
    host_name=''
  else
    if [ -z "$host_name" ]
    then
      echo -n "* Looking up the hostname of this machine: "
      host_name=$(hostname -f)
      echo "$host_name"
    fi
    echo -en "Set hostname to \"\E[92m$host_name"
    echo -ne "\E[0m\"? (Y)es/(N)o/(E)nter/(A)bort\n> "
    read -re command
    case $command in
      y | Y)
        host_name_ok="yes"
        ;;
      n | N)
        host_name=''
        ;;
      e | E)
        echo "Enter hostname: "
        read -re host_name
        ;;
      a | A)
        exit 1
        ;;
    esac
  fi
done
echo "* Writing environment file $env_file."
echo "HOST_NAME=$host_name" >.env

echo "
Since we don't use a certificate for our setup, we have to add the container
registry to the docker configuration to be able to access it anyways.
"

docker_dir="/etc/docker"
docker_config="$docker_dir/daemon.json"
config_json="{\"insecure-registries\" : [\"$host_name:5000\"]}"
echo "* Checking if $docker_config already exist"
if "$PRIVILEGE_ESCALATION_CMD" bash -c "[ -d \"$docker_dir\" ] && [ -f \"$docker_config\" ]"
then
  echo "$docker_config already exists"
  if type jq >/dev/null 2>&1
  then
    echo "Found jq for merge option into existing configuration"
    combined_config=$(echo "$config_json" | "$PRIVILEGE_ESCALATION_CMD" jq -s '.[0] * .[1]' - "$docker_config")
    echo -e "The configuration would be\n\E[94m$combined_config\E[0m"
    interactive_continue "Write this to $docker_config?"
    echo "$combined_config" | "$PRIVILEGE_ESCALATION_CMD" tee "$docker_config" >/dev/null
  else
    echo "Please add the following configuration to $docker_config manually."
    echo -e "\n\E[94m$config_json\E[0m\n"
    interactive_continue "If the modifications are made, continue?"
  fi
else
  echo "$docker_config doesn't exist, yet."
  echo "* Writing new configuration"
  "$PRIVILEGE_ESCALATION_CMD" mkdir -p "$docker_dir"
  echo "$config_json" | "$PRIVILEGE_ESCALATION_CMD" tee "$docker_config" >/dev/null
fi

if interactive_continue "Restart docker service?"
then
  echo "* Restarting docker service"
  "$PRIVILEGE_ESCALATION_CMD" systemctl restart docker.service
fi

echo -e "\n[\E[93mStep 2\E[0m] Setup Containers"

interactive_continue "We're about to pull the container images and start the containers. Continue?"

echo "* Pulling container images"
docker-compose pull
echo "* Starting containers"
docker-compose up -d

echo -e "\n[\E[93mStep 3\E[0m] Configure Gitlab and Gitlab CI Runner"

while ! docker exec -it gitlab-ci-gitlab test -f /etc/gitlab/gitlab.rb
do
  echo "* Waiting for Gitlab config file being created"
  sleep 1
done

echo "* Configure Gitlab to recognize its SSH port as $SSH_PORT"
docker exec -it gitlab-ci-gitlab sed -i "s/# gitlab_rails\['gitlab_shell_ssh_port'\] = 22/gitlab_rails\['gitlab_shell_ssh_port'\] = 2221/" /etc/gitlab/gitlab.rb
echo "* Configure Gitlab external_url to \"http://$host_name:$HTTP_PORT\""
docker exec -it gitlab-ci-gitlab sed -i "s/# external_url 'GENERATED_EXTERNAL_URL'/external_url 'http:\/\/$host_name:$HTTP_PORT'/" /etc/gitlab/gitlab.rb

echo -e "This is the effective configuration:\E[94m"
docker exec -it gitlab-ci-gitlab cat /etc/gitlab/gitlab.rb | grep -vP '^#|^\s$'
interactive_continue "\E[0mFor the configuration to take effect the gitlab container has to be restarted. Continue?"
docker container restart gitlab-ci-gitlab

echo -e "Now all containers should be running and becoming ready. Use \E[94mdocker-compose ps\E[0m to get a status overview:\E[96m"
docker-compose ps
echo -e "\E[0m"

echo "The initial password for the administrator account will be stored at /etc/gitlab/initial_root_password."
echo -e "Display the content with the command: \E[94mdocker exec gitlab-ci-gitlab cat /etc/gitlab/initial_root_password\E[0m"
echo "Now open \"http://$host_name:$HTTP_PORT\" and login with user 'root' and the password shown above!"
echo "Continue now with runner registration!"
